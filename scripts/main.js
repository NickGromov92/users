'use strict';

let users = {};

function User() {
  let userName = '';
  let userId = null;
  let card = document.createElement('div');
  let removeBtn = document.createElement('button');
  let p = document.createElement('p');

  function renameUser(id) {
    createButton.style.display = 'none';
    renameButton.style.display = 'inline-block';
    nameField.value = userName;

    renameButton.addEventListener('click', function(event) {
      event.preventDefault();
      userName = nameField.value;
      p.textContent = nameField.value;
    });
  }

  function createCard() {
    card.id = userId;
    card.classList.add('card');

    p.textContent = userName;
    card.appendChild(p);

    card.textContent = userName;
    let usersBlock = document.querySelector('#usersBlock');
    usersBlock.appendChild(card);

    removeBtn.classList.add('removeBtn');
    removeBtn.textContent = 'REMOVE';

    let renameBtn = document.createElement('button');
    renameBtn.classList.add('renameBtn');
    renameBtn.textContent = 'RENAME';

    let renameCallback = function(event) {
      event.preventDefault();
      let userId = this.parentNode.id;
      renameUser(userId);
    };

    let removeCallback = function(event) {
      event.preventDefault();
      this.parentNode.remove();
    };

    removeBtn.addEventListener('click', removeCallback);
    renameBtn.addEventListener('click', renameCallback);

    card.appendChild(removeBtn);
    card.appendChild(renameBtn);
  }


  return {
    addUser: function(name, id) {
      userName = name;
      userId = id;
      createCard();
    },

    removeUser: function(id) {
      document.querySelector('#' + id);
    },
  };
}

let createButton = document.querySelector('#createButton');
let renameButton = document.querySelector('#renameButton');
let nameField = document.querySelector('#lastName');

createButton.addEventListener('click', function(event) {
  event.preventDefault();

  let nameValue = nameField.value;
  let date = new Date;
  let hash = date.getTime();

  let resultName = 'user' + hash;

  users[resultName] = User();
  users[resultName].addUser(nameValue, resultName);


  nameField.value = '';
});


